import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';

import { ToDo, TodoAdapter } from '../app/core/todo.model';
import { ToDoApiService } from './to-do-api.service';

@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  constructor(
    private http: HttpClient,
    private toDoApiService: ToDoApiService,
    private adapter: TodoAdapter
  ) { }

  getToDo = (id: string): Observable<ToDo> => this.http.get<object>(`${this.toDoApiService.TODOS}/${id}`)
    .pipe(
      map((item: object) => this.adapter.adapt(item))
    )

  getToDos = (): Observable<Array<ToDo>> => this.http.get<Array<object>>(this.toDoApiService.TODOS)
    .pipe(
      map((data: Array<object>) => data.map(item => this.adapter.adapt(item))),
      // map((todos: Array<ToDo>) => todos.reduce((acc: Array<ToDo>, todo: ToDo) => {
      //   if (!todo.completed) {
      //     acc.push(todo);
      //   }
      //   return acc;
      // }, [])),
      // tap(item => console.log('item', item))
    )

  createToDo = (todo: ToDo) => this.http.post<ToDo>(this.toDoApiService.TODOS, todo);

  updateToDo = (todo: ToDo) => this.http.put<ToDo>(`${this.toDoApiService.TODOS}/${todo.id}`, todo);

  deleteToDo = (todo: ToDo) => this.http.delete(`${this.toDoApiService.TODOS}/${todo.id}`);
}
