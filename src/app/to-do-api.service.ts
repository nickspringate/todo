import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ToDoApiService {

  private readonly baseApiUrl: string;
  public readonly TODOS: string;

  constructor() {
    this.baseApiUrl = environment.baseApiUrl;
    this.TODOS = `${this.baseApiUrl}/todos`;
  }
}
