import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NgbDateStruct, NgbTimeStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';

import { ToDoService } from '../to-do.service';
import { ToDo } from '../core/todo.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {

  minDate: NgbDateStruct;
  toDoDate: NgbDateStruct;
  toDoTime: NgbTimeStruct;
  toDoTitle: string;
  meridian: boolean;
  todo: ToDo;

  constructor(
    private calendar: NgbCalendar,
    private toDoService: ToDoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.reset();
  }

  private getToDoTime = (todo?: ToDo) => {
    const m = todo ? moment(todo.dueDate) : moment();
    return {
      hour: m.hour(),
      minute: m.minute(),
      second: m.second()
    };
  }

  private getToDoDate = (todo?: ToDo) => {
    const m = todo ? moment(todo.dueDate) : moment();
    return {
      year: m.year(),
      month: m.month(),
      day: m.date()
    };
  }

  private reset = () => {
    this.meridian = true;
    this.toDoDate = this.calendar.getToday();
    this.toDoTime = this.getToDoTime();
    this.minDate = this.calendar.getToday();
  }

  private getToDoDueDate = () => {
    return moment()
      .year(this.toDoDate.year)
      .month(this.toDoDate.month)
      .date(this.toDoDate.day)
      .hour(this.toDoTime.hour)
      .minute(this.toDoTime.minute)
      .second(this.toDoTime.second)
      .toDate();
  }

  private prepareToDo = () => {
    let t: ToDo;
    const toDoDueDate = this.getToDoDueDate();
    if (this.todo) {
      t = Object.assign(this.todo, new ToDo(this.toDoTitle, this.todo.completed, this.todo.id, toDoDueDate));
    } else {
      t = new ToDo(this.toDoTitle, false, null, toDoDueDate);
    }
    return t;
  }

  ngOnInit() {
    // tslint:disable-next-line:no-string-literal
    const id = this.activatedRoute.snapshot.params['id'];
    if (id && 'new' !== id) {
      this.toDoService.getToDo(id)
        .subscribe(
          (todo: ToDo) => {
            this.todo = todo;
            this.toDoTitle = this.todo.title;
            this.toDoDate = this.getToDoDate(this.todo);
            this.toDoTime = this.getToDoTime(this.todo);
          },
          this.reset
        );
    } else {
      this.reset();
    }
  }

  onSubmit = () => {
    const todo = this.prepareToDo();
    let subsctiption: Observable<any>;

    if (todo.id) {
      subsctiption = this.toDoService.updateToDo(todo);
    } else {
      subsctiption = this.toDoService.createToDo(todo);
    }

    subsctiption.subscribe(
      _ => this.router.navigate(['/']),
      console.error
    );
  }

}
