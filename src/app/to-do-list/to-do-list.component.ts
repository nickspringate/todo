import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { switchMap } from 'rxjs/operators';

import { ToDoService } from '../to-do.service';
import { ToDo } from '../core/todo.model';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  todos: Array<ToDo>;

  constructor(
    private toDoService: ToDoService,
    private router: Router
  ) {
    this.todos = [];
  }

  ngOnInit() {
    this.toDoService.getToDos()
      .subscribe(
        (todos: Array<ToDo>) => this.todos = todos
      );
  }

  onEdit = (todo: ToDo) => {
    this.router.navigate(['/list', todo.id]);
  }

  onComplete = (todo: ToDo) => {
    todo = Object.assign(todo, { completed: true });

    this.toDoService.updateToDo(todo)
      .pipe(
        switchMap(res => this.toDoService.getToDos())
      )
      .subscribe(
        (todos: Array<ToDo>) => this.todos = todos,
        console.error
      );
  }

  onDelete = (todo: ToDo) => {

    this.toDoService.deleteToDo(todo)
      .pipe(
        switchMap(res => this.toDoService.getToDos())
      )
      .subscribe(
        (todos: Array<ToDo>) => this.todos = todos,
        console.error
      );
  }

}
