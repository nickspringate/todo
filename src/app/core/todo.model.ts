import { Injectable } from '@angular/core';

import { Adapter } from './adapter';

export class ToDo {
  constructor(
    public title: string,
    public completed: boolean,
    public id?: string,
    public dueDate?: Date
  ) {
    if (id) {
      this.id = id;
    }
    this.title = title;
    this.completed = completed;
    if (dueDate) {
      this.dueDate = new Date(dueDate);
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class TodoAdapter implements Adapter<ToDo> {

  adapt(item: any): ToDo {
    return new ToDo(
      item.title,
      item.completed,
      item._id,
      item.dueDate
    );
  }
}
